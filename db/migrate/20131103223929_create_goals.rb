class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.integer :user_id
      t.integer :challenger_id
      t.string :name
      t.integer :num_of_steps
      t.integer :num_of_steps_so_far
      t.float :dollars_pledge
      t.integer :charity_id
      t.integer :goaltype_id
      t.integer :accept
      t.integer :confirm_to_charity
      t.float :amount_of_transaction
      t.date :date_of_transaction

      t.timestamps
    end
  end
end
