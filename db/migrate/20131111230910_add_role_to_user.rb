class AddRoleToUser < ActiveRecord::Migration
  def change
      add_column :users, :role, :integer, :default=>User::ROLE_USER
  end
end
