# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.destroy_all
Goal.destroy_all

fa1 = User.new(:first_name => 'Gap', :last_name => 'Supaongprapa', :username => 'gap', :password => 'SU2orange!', :password_confirmation => 'SU2orange!', :role=>User::ROLE_ADMIN)
fa1.image = File.open('free/m/gap.jpg')
fa1.save

# open
g1=Goal.new(:user_id=>fa1.id, :challenger_id=>fa1.id, :name=>'My first goal', :num_of_steps=>10000, :num_of_steps_so_far=>0)
g1.save
# in progress (already accepted)
g2=Goal.new(:user_id=>fa1.id, :challenger_id=>fa1.id, :name=>'Journey to health', :num_of_steps=>20000, :num_of_steps_so_far=>10000, :accept=>1)
g2.save
# completed
g3=Goal.new(:user_id=>fa1.id, :challenger_id=>fa1.id, :name=>'Walking in the snow', :num_of_steps=>5000, :num_of_steps_so_far=>5050)
g3.save
# rejected
g4=Goal.new(:user_id=>fa1.id, :challenger_id=>fa1.id, :name=>'Too hot to walk today', :num_of_steps=>50, :num_of_steps_so_far=>0, :accept=>0)
g4.save

fa2 = User.new(:first_name => 'Rudy', :last_name => 'Rusli', :username => 'rarusli', :password => 'SU2orange!', :password_confirmation => 'SU2orange!', :role => User::ROLE_ADMIN)
fa2.image = File.open('free/m/rudy.jpg')
fa2.save
g4=Goal.new(:user_id=>fa2.id, :challenger_id=>fa2.id, :name=>'Yeayyy !!', :num_of_steps=>40000, :num_of_steps_so_far=>10000, :accept=>1)
g4.save

f1 = User.new(:first_name => 'Sophia', :last_name => 'Smith', :username => 'sophiasmith', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f1.image = File.open('free/f/1.jpg')
f1.save
g5=Goal.new(:user_id=>f1.id, :challenger_id=>f1.id, :name=>'Sophia\'s Journey', :num_of_steps=>80000, :num_of_steps_so_far=>60000, :accept=>1)
g5.save

m1 = User.new(:first_name => 'Jacob', :last_name => 'Johnson', :username => 'jacobjohnson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m1.image = File.open('free/m/1.jpg')
m1.save
g6=Goal.new(:user_id=>m1.id, :challenger_id=>m1.id, :name=>'JJ\'s First goal', :num_of_steps=>100000, :num_of_steps_so_far=>66000, :accept=>1)
g6.save

f2 = User.new(:first_name => 'Isabella', :last_name => 'Williams', :username => 'isabellawilliams', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f2.image = File.open('free/f/2.jpg')
f2.save

m2 = User.new(:first_name => 'Mason', :last_name => 'Brown', :username => 'masonbrown', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m2.image = File.open('free/m/2.jpg')
m2.save

f3 = User.new(:first_name => 'Emma', :last_name => 'Jones', :username => 'emmajones', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f3.image = File.open('free/f/3.jpg')
f3.save

m3 = User.new(:first_name => 'William', :last_name => 'Miller', :username => 'williammiller', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m3.image = File.open('free/m/3.jpg')
m3.save

f4 = User.new(:first_name => 'Olivia', :last_name => 'Davis', :username => 'oliviadavis', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f4.image = File.open('free/f/4.jpg')
f4.save

m4 = User.new(:first_name => 'Jayden', :last_name => 'Garcia', :username => 'jaydengarcia', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m4.image = File.open('free/m/4.jpg')
m4.save

f5 = User.new(:first_name => 'Ava ', :last_name => 'Rodriguez', :username => 'avrodriguez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f5.image = File.open('free/f/5.jpg')
f5.save

m5 = User.new(:first_name => 'Noah', :last_name => 'Wilson', :username => 'noahwilson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m5.image = File.open('free/m/5.jpg')
m5.save

f6 = User.new(:first_name => 'Emily', :last_name => 'Martinez', :username => 'emilymartinez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f6.image = File.open('free/f/6.jpg')
f6.save

m6 = User.new(:first_name => 'Michael', :last_name => 'Anderson', :username => 'michaelanderson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m6.image = File.open('free/m/6.jpg')
m6.save

f7 = User.new(:first_name => 'Abigail', :last_name => 'Taylor', :username => 'abigailtaylor', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f7.image = File.open('free/f/7.jpg')
f7.save

m7 = User.new(:first_name => 'Ethan', :last_name => 'Thomas', :username => 'ethanthomas', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m7.image = File.open('free/m/7.jpg')
m7.save

f8 = User.new(:first_name => 'Madison', :last_name => 'Hernandez', :username => 'madisonhernandez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f8.image = File.open('free/f/8.jpg')
f8.save

m8 = User.new(:first_name => 'Alexander', :last_name => 'Moore', :username => 'alexandermoore', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m8.image = File.open('free/m/8.jpg')
m8.save

f9 = User.new(:first_name => 'Mia ', :last_name => 'Martin', :username => 'mimartin', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f9.image = File.open('free/f/9.jpg')
f9.save

m9 = User.new(:first_name => 'Aiden', :last_name => 'Jackson', :username => 'aidenjackson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m9.image = File.open('free/m/9.jpg')
m9.save

f10 = User.new(:first_name => 'Chloe', :last_name => 'Thompson', :username => 'chloethompson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f10.image = File.open('free/f/10.jpg')
f10.save

m10 = User.new(:first_name => 'Daniel', :last_name => 'White', :username => 'danielwhite', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m10.image = File.open('free/m/10.jpg')
m10.save

f11 = User.new(:first_name => 'Elizabeth', :last_name => 'Lopez', :username => 'elizabethlopez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f11.image = File.open('free/f/11.jpg')
f11.save

m11 = User.new(:first_name => 'Anthony', :last_name => 'Lee', :username => 'anthonylee', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m11.image = File.open('free/m/11.jpg')
m11.save

f12 = User.new(:first_name => 'Ella', :last_name => 'Gonzalez', :username => 'ellagonzalez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f12.image = File.open('free/f/12.jpg')
f12.save

m12 = User.new(:first_name => 'Matthew', :last_name => 'Harris', :username => 'matthewharris', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m12.image = File.open('free/m/12.jpg')
m12.save

f13 = User.new(:first_name => 'Addison', :last_name => 'Clark', :username => 'addisonclark', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f13.image = File.open('free/f/13.jpg')
f13.save

m13 = User.new(:first_name => 'Elijah', :last_name => 'Lewis', :username => 'elijahlewis', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m13.image = File.open('free/m/13.jpg')
m13.save

f14 = User.new(:first_name => 'Natalie', :last_name => 'Robinson', :username => 'natalierobinson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f14.image = File.open('free/f/14.jpg')
f14.save

m14 = User.new(:first_name => 'Joshua', :last_name => 'Walker', :username => 'joshuawalker', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m14.image = File.open('free/m/14.jpg')
m14.save

f15 = User.new(:first_name => 'Lily', :last_name => 'Perez', :username => 'lilyperez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f15.image = File.open('free/f/15.jpg')
f15.save

m15 = User.new(:first_name => 'Liam', :last_name => 'Hall', :username => 'liamhall', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m15.image = File.open('free/m/15.jpg')
m15.save

f16 = User.new(:first_name => 'Grace', :last_name => 'Young', :username => 'graceyoung', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f16.image = File.open('free/f/16.jpg')
f16.save

m16 = User.new(:first_name => 'Andrew', :last_name => 'Allen', :username => 'andrewallen', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m16.image = File.open('free/m/16.jpg')
m16.save

f17 = User.new(:first_name => 'Samantha', :last_name => 'Sanchez', :username => 'samanthasanchez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f17.image = File.open('free/f/17.jpg')
f17.save

m17 = User.new(:first_name => 'James', :last_name => 'Wright', :username => 'jameswright', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m17.image = File.open('free/m/17.jpg')
m17.save

f18 = User.new(:first_name => 'Avery', :last_name => 'King', :username => 'averyking', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f18.image = File.open('free/f/18.jpg')
f18.save

m18 = User.new(:first_name => 'David', :last_name => 'Scott', :username => 'davidscott', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m18.image = File.open('free/m/18.jpg')
m18.save

f19 = User.new(:first_name => 'Sofia', :last_name => 'Green', :username => 'sofiagreen', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f19.image = File.open('free/f/19.jpg')
f19.save

m19 = User.new(:first_name => 'Benjamin', :last_name => 'Baker', :username => 'benjaminbaker', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m19.image = File.open('free/m/19.jpg')
m19.save

f20 = User.new(:first_name => 'Aubrey', :last_name => 'Adams', :username => 'aubreyadams', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f20.image = File.open('free/f/20.jpg')
f20.save

m20 = User.new(:first_name => 'Logan', :last_name => 'Nelson', :username => 'logannelson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m20.image = File.open('free/m/20.jpg')
m20.save

f21 = User.new(:first_name => 'Brooklyn', :last_name => 'Hill', :username => 'brooklynhill', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f21.image = File.open('free/f/21.jpg')
f21.save

m21 = User.new(:first_name => 'Christopher', :last_name => 'Ramirez', :username => 'christopherramirez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m21.image = File.open('free/m/21.jpg')
m21.save

f22 = User.new(:first_name => 'Lillian', :last_name => 'Campbell', :username => 'lilliancampbell', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f22.image = File.open('free/f/22.jpg')
f22.save

m22 = User.new(:first_name => 'Joseph', :last_name => 'Mitchell', :username => 'josephmitchell', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m22.image = File.open('free/m/22.jpg')
m22.save

f23 = User.new(:first_name => 'Victoria', :last_name => 'Roberts', :username => 'victoriaroberts', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f23.image = File.open('free/f/23.jpg')
f23.save

m23 = User.new(:first_name => 'Jackson', :last_name => 'Carter', :username => 'jacksoncarter', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m23.image = File.open('free/m/23.jpg')
m23.save

f24 = User.new(:first_name => 'Evelyn', :last_name => 'Phillips', :username => 'evelynphillips', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f24.image = File.open('free/f/24.jpg')
f24.save

m24 = User.new(:first_name => 'Gabriel', :last_name => 'Evans', :username => 'gabrielevans', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m24.image = File.open('free/m/24.jpg')
m24.save

f25 = User.new(:first_name => 'Hannah', :last_name => 'Turner', :username => 'hannahturner', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f25.image = File.open('free/f/25.jpg')
f25.save

m25 = User.new(:first_name => 'Ryan', :last_name => 'Torres', :username => 'ryantorres', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m25.image = File.open('free/m/25.jpg')
m25.save

f26 = User.new(:first_name => 'Alexis', :last_name => 'Parker', :username => 'alexisparker', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f26.image = File.open('free/f/26.jpg')
f26.save

m26 = User.new(:first_name => 'Samuel', :last_name => 'Collins', :username => 'samuelcollins', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m26.image = File.open('free/m/26.jpg')
m26.save

f27 = User.new(:first_name => 'Charlotte', :last_name => 'Edwards', :username => 'charlotteedwards', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f27.image = File.open('free/f/27.jpg')
f27.save

m27 = User.new(:first_name => 'John', :last_name => 'Stewart', :username => 'johnstewart', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m27.image = File.open('free/m/27.jpg')
m27.save

f28 = User.new(:first_name => 'Zoey', :last_name => 'Flores', :username => 'zoeyflores', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f28.image = File.open('free/f/28.jpg')
f28.save

m28 = User.new(:first_name => 'Nathan', :last_name => 'Morris', :username => 'nathanmorris', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m28.image = File.open('free/m/28.jpg')
m28.save

f29 = User.new(:first_name => 'Leah', :last_name => 'Nguyen', :username => 'leahnguyen', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f29.image = File.open('free/f/29.jpg')
f29.save

m29 = User.new(:first_name => 'Lucas', :last_name => 'Murphy', :username => 'lucasmurphy', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m29.image = File.open('free/m/29.jpg')
m29.save

f30 = User.new(:first_name => 'Amelia', :last_name => 'Rivera', :username => 'ameliarivera', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f30.image = File.open('free/f/30.jpg')
f30.save

m30 = User.new(:first_name => 'Christian', :last_name => 'Cook', :username => 'christiancook', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m30.image = File.open('free/m/30.jpg')
m30.save

f31 = User.new(:first_name => 'Zoe', :last_name => 'Rogers', :username => 'zoerogers', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f31.image = File.open('free/f/31.jpg')
f31.save

m31 = User.new(:first_name => 'Jonathan', :last_name => 'Morgan', :username => 'jonathanmorgan', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m31.image = File.open('free/m/31.jpg')
m31.save

f32 = User.new(:first_name => 'Hailey', :last_name => 'Peterson', :username => 'haileypeterson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f32.image = File.open('free/f/32.jpg')
f32.save

m32 = User.new(:first_name => 'Caleb', :last_name => 'Cooper', :username => 'calebcooper', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m32.image = File.open('free/m/32.jpg')
m32.save

f33 = User.new(:first_name => 'Layla', :last_name => 'Reed', :username => 'laylareed', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f33.image = File.open('free/f/33.jpg')
f33.save

m33 = User.new(:first_name => 'Dylan', :last_name => 'Bailey', :username => 'dylanbailey', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m33.image = File.open('free/m/33.jpg')
m33.save

f34 = User.new(:first_name => 'Gabriella', :last_name => 'Bell', :username => 'gabriellabell', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f34.image = File.open('free/f/34.jpg')
f34.save

m34 = User.new(:first_name => 'Landon', :last_name => 'Gomez', :username => 'landongomez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m34.image = File.open('free/m/34.jpg')
m34.save

f35 = User.new(:first_name => 'Nevaeh', :last_name => 'Kelly', :username => 'nevaehkelly', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f35.image = File.open('free/f/35.jpg')
f35.save

m35 = User.new(:first_name => 'Isaac', :last_name => 'Howard', :username => 'isaachoward', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m35.image = File.open('free/m/35.jpg')
m35.save

f36 = User.new(:first_name => 'Kaylee', :last_name => 'Ward', :username => 'kayleeward', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f36.image = File.open('free/f/36.jpg')
f36.save

m36 = User.new(:first_name => 'Gavin', :last_name => 'Cox', :username => 'gavincox', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m36.image = File.open('free/m/36.jpg')
m36.save

f37 = User.new(:first_name => 'Alyssa', :last_name => 'Diaz', :username => 'alyssadiaz', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f37.image = File.open('free/f/37.jpg')
f37.save

m37 = User.new(:first_name => 'Brayden', :last_name => 'Richardson', :username => 'braydenrichardson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m37.image = File.open('free/m/37.jpg')
m37.save

f38 = User.new(:first_name => 'Anna', :last_name => 'Wood', :username => 'annawood', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f38.image = File.open('free/f/38.jpg')
f38.save

m38 = User.new(:first_name => 'Tyler', :last_name => 'Watson', :username => 'tylerwatson', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m38.image = File.open('free/m/38.jpg')
m38.save

f39 = User.new(:first_name => 'Sarah', :last_name => 'Brooks', :username => 'sarahbrooks', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f39.image = File.open('free/f/39.jpg')
f39.save

m39 = User.new(:first_name => 'Luke', :last_name => 'Bennett', :username => 'lukebennett', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m39.image = File.open('free/m/39.jpg')
m39.save

f40 = User.new(:first_name => 'Allison', :last_name => 'Gray', :username => 'allisongray', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f40.image = File.open('free/f/40.jpg')
f40.save

m40 = User.new(:first_name => 'Evan', :last_name => 'James', :username => 'evanjames', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m40.image = File.open('free/m/40.jpg')
m40.save

f41 = User.new(:first_name => 'Savannah', :last_name => 'Reyes', :username => 'savannahreyes', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f41.image = File.open('free/f/41.jpg')
f41.save

m41 = User.new(:first_name => 'Carter', :last_name => 'Cruz', :username => 'cartercruz', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m41.image = File.open('free/m/41.jpg')
m41.save

f42 = User.new(:first_name => 'Ashley', :last_name => 'Hughes', :username => 'ashleyhughes', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f42.image = File.open('free/f/42.jpg')
f42.save

m42 = User.new(:first_name => 'Nicholas', :last_name => 'Price', :username => 'nicholasprice', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m42.image = File.open('free/m/42.jpg')
m42.save

f43 = User.new(:first_name => 'Audrey', :last_name => 'Myers', :username => 'audreymyers', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f43.image = File.open('free/f/43.jpg')
f43.save

m43 = User.new(:first_name => 'Isaiah', :last_name => 'Long', :username => 'isaiahlong', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m43.image = File.open('free/m/43.jpg')
m43.save

f44 = User.new(:first_name => 'Taylor', :last_name => 'Foster', :username => 'taylorfoster', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f44.image = File.open('free/f/44.jpg')
f44.save

m44 = User.new(:first_name => 'Owen', :last_name => 'Sanders', :username => 'owensanders', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m44.image = File.open('free/m/44.jpg')
m44.save

f45 = User.new(:first_name => 'Brianna', :last_name => 'Ross', :username => 'briannaross', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f45.image = File.open('free/f/45.jpg')
f45.save

m45 = User.new(:first_name => 'Jack', :last_name => 'Morales', :username => 'jackmorales', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m45.image = File.open('free/m/45.jpg')
m45.save

f46 = User.new(:first_name => 'Aaliyah', :last_name => 'Powell', :username => 'aaliyahpowell', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f46.image = File.open('free/f/46.jpg')
f46.save

m46 = User.new(:first_name => 'Jordan', :last_name => 'Sullivan', :username => 'jordansullivan', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m46.image = File.open('free/m/46.jpg')
m46.save

f47 = User.new(:first_name => 'Riley', :last_name => 'Russell', :username => 'rileyrussell', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f47.image = File.open('free/f/47.jpg')
f47.save

m47 = User.new(:first_name => 'Brandon', :last_name => 'Ortiz', :username => 'brandonortiz', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m47.image = File.open('free/m/47.jpg')
m47.save

f48 = User.new(:first_name => 'Camila', :last_name => 'Jenkins', :username => 'camilajenkins', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f48.image = File.open('free/f/48.jpg')
f48.save

m48 = User.new(:first_name => 'Wyatt', :last_name => 'Gutierrez', :username => 'wyattgutierrez', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m48.image = File.open('free/m/48.jpg')
m48.save

f49 = User.new(:first_name => 'Khloe', :last_name => 'Perry', :username => 'khloeperry', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f49.image = File.open('free/f/49.jpg')
f49.save

m49 = User.new(:first_name => 'Julian', :last_name => 'Butler', :username => 'julianbutler', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
m49.image = File.open('free/m/49.jpg')
m49.save

f50 = User.new(:first_name => 'Claire', :last_name => 'Barnes', :username => 'clairebarnes', :password => 'SU2orange!', :password_confirmation => 'SU2orange!')
f50.image = File.open('free/f/50.jpg')
f50.save