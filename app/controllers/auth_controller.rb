class AuthController < ApplicationController
  def login
    if request.post?
      user = User.find_by_username(params[:username])
      if user && user.password == params[:password]
        session[:user_id] = user.id
        redirect_to :controller => 'goals', :action => 'index'
      else
        flash[:error] = "Invalid user name/password." + params[:username]
        redirect_to :back
      end
    end
  end

  def logout
    session[:user_id] = nil
    redirect_to :controller => 'auth', :action => 'login'
  end
  
  def newuser
    
  end
  
  def signup_new_user
    newuser_hash = Hash.new
    newuser_hash["first_name"] = params[:first_name]
    newuser_hash["last_name"] = params[:last_name]
    newuser_hash["username"] =  params[:username]
    newuser_hash["password"] =  params[:password]
  
    #render :text => newuser_hash
    user = User.find_by_username(
      newuser_hash["username"]) || User.create_new_user(newuser_hash)
    session[:user_id] = user.id
    redirect_to root_url, :notice => "Signed in!"
  end
end
