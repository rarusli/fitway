class GoalupdaterController < ApplicationController
  def update_goal
    @user_id = params[:user_id]
    @goal_id = params[:goal_id]
    @num_of_steps = params[:num_of_steps]
    
    goal=Goal.find_by_id(params[:goal_id])
    goal.update_attributes(:num_of_steps_so_far=>params[:num_of_steps])
  end
end
