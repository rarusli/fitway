class GoalsController < ApplicationController
  before_filter :authorize_user_self_or_admin, :only => [:edit, :update, :destroy]
  helper_method :user_self?

  def authorize_user_self_or_admin
    if !user_self_or_admin? 
      flash[:error] = 'The action is only allowed to the user him/ herself.'
      redirect_to :back
    end
  end

  def user_self_or_admin?
    goal = Goal.find(params[:id])
    user_session = User.find(session[:user_id])
    user_param_id = goal.user_id
    user_session and user_param_id and 
      ((user_session.id == user_param_id) or user_session.role==User::ROLE_ADMIN)
  end
  
  # GET /goals
  # GET /goals.json
  def index
    @goals = Goal.all(:order=>"updated_at DESC")
    @goals_acceptedOrRejected = Goal.where("accept=1 or accept=0").order("updated_at DESC")
    @goals_notYetAcceptedOrRejected = Goal.where(accept: nil, user_id: session[:user_id]).order("updated_at DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @goals }
    end
  end

  # GET /goals/1
  # GET /goals/1.json
  def show
    @goal = Goal.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @goal }
    end
  end

  # GET /goals/new
  # GET /goals/new.json
  def new
    @goal = Goal.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @goal }
    end
  end

  # GET /goals/1/edit
  def edit
    @goal = Goal.find(params[:id])
  end

  # POST /goals
  # POST /goals.json
  def create
    @goal = Goal.new(params[:goal])

    respond_to do |format|
      if @goal.save
        format.html { redirect_to @goal, notice: 'Goal was successfully created.' }
        format.json { render json: @goal, status: :created, location: @goal }
      else
        format.html { render action: "new" }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /goals/1
  # PUT /goals/1.json
  def update
    @goal = Goal.find(params[:id])

    respond_to do |format|
      if @goal.update_attributes(params[:goal])
        format.html { redirect_to @goal, notice: 'Goal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1
  # DELETE /goals/1.json
  def destroy
    @goal = Goal.find(params[:id])
    @goal.destroy

    respond_to do |format|
      format.html { redirect_to goals_url }
      format.json { head :no_content }
    end
  end
  
end
