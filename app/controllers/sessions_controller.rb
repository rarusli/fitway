class SessionsController < ApplicationController
    def new
    end
    
    def create_social_login
        auth_hash = request.env["omniauth.auth"]
        provider = auth_hash["provider"]
        
        if provider.nil?
          redirect_to root_url, :notice => "Provider is not specified"
        elsif provider == 'facebook'
          user = User.find_by_provider_and_uid(auth_hash["provider"], auth_hash["uid"]) || User.create_with_fb_omniauth(auth_hash)
          session[:user_id] = user.id
          redirect_to root_url, :notice => "Signed in!"
        elsif provider == 'google_oauth2'
          user = User.find_by_provider_and_uid(auth_hash["provider"], auth_hash["uid"]) || User.create_with_google_omniauth(auth_hash)
          session[:user_id] = user.id
          redirect_to root_url, :notice => "Signed in!"
        else 
          redirect_to root_url, :notice => "Provider is not supported yet"
        end
        # for debugging
        #render :text => auth_hash.inspect
        # for google
        #render :text => auth_hash["info"]["first_name"] + "--" + auth_hash["info"]["last_name"] + "--" + auth_hash["info"]["email"] + 
        #  "--" + auth_hash["info"]["image"] # from google 
    end
    
    # no need, we can just use signout provided by regular log out.
    #def destroy_social_login
    #    session[:user_id] = nil
    #    redirect_to root_url, :notice => "Signed out!"
    #end
    
    def failure
        flash[:error] = 'Login failed.'
        redirect_to root_url
    end
end