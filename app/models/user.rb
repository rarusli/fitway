class User < ActiveRecord::Base
  ROLE_USER = 1
  ROLE_ADMIN = 2

  attr_accessible :first_name, :last_name, :num_of_steps, :password, :username
  attr_accessible :password_confirmation, :role, :provider, :uid

  attr_accessible :image
  mount_uploader :image, ImageUploader

  has_many :goals, :dependent=>:destroy

  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :username, :presence => true, :uniqueness => true
  #validates :password, :presence => true, :confirmation => true
  #validates :username, :uniqueness => true
  validates :password, :confirmation => true
  
  def self.create_with_fb_omniauth(auth_hash)
    create! do |user|
        user.provider = auth_hash["provider"]
        user.uid = auth_hash["uid"]
        user.username = auth_hash["info"]["nickname"]
        user.first_name = auth_hash["info"]["first_name"]
        user.last_name = auth_hash["info"]["last_name"]
    end
  end
  
  def self.create_with_google_omniauth(auth_hash)
    create! do |user|
        user.provider = auth_hash["provider"]
        user.uid = auth_hash["uid"]
        user.username = auth_hash["info"]["email"]
        user.first_name = auth_hash["info"]["first_name"]
        user.last_name = auth_hash["info"]["last_name"]
        user.remote_image_url = auth_hash["info"]["image"]
    end
  end
  
  def self.create_new_user(newuser_hash)
    create! do |user|
        user.first_name = newuser_hash["first_name"]
        user.last_name = newuser_hash["last_name"]
        user.username = newuser_hash["username"]
        user.password = newuser_hash["password"]
    end
  end

  def get_total_steps_so_far
    total_goals = 0
    goals.each do |goal|
       total_goals += goal.num_of_steps_so_far
    end
    total_goals
  end
  
  def self.search(search)
    if search
      where('first_name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end
  
  def is_admin?
    !!(role == ROLE_ADMIN)
  end

end
