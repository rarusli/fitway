class Goal < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  require 'active_support/core_ext'
  
  attr_accessible :accept, :amount_of_transaction, :challenger_id, :charity_id,
  :confirm_to_charity, :date_of_transaction, :dollars_pledge, :goaltype_id, :name, :num_of_steps, :num_of_steps_so_far, :user_id, :updated_at

  belongs_to :user
  belongs_to :charity
  belongs_to :goaltype

  validates :user_id, :presence => true
  validates :challenger_id, :presence => true
  validates :name, :presence => true
  validates :num_of_steps, :presence => true
  
  before_save :default_values
  
  def default_values
    self.num_of_steps_so_far ||= 0
  end
  
  def percent_reached_so_far
    (num_of_steps_so_far.to_f/ num_of_steps)*100
  end
  
  def num_steps_formatted
    number_with_delimiter(num_of_steps, delimiter:",")
  end
  
  def num_steps_so_far_formatted
    number_with_delimiter(num_of_steps_so_far, delimiter:",")
  end
end
